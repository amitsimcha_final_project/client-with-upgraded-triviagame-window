﻿namespace loadScrean
{
    partial class triviaGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ExitButton = new System.Windows.Forms.Button();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.RoomNameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Answer1Button = new System.Windows.Forms.Button();
            this.Answer2Button = new System.Windows.Forms.Button();
            this.Answer3Button = new System.Windows.Forms.Button();
            this.Answer4Button = new System.Windows.Forms.Button();
            this.NumOfQuestionsLabel = new System.Windows.Forms.Label();
            this.QuestionTimerLable = new System.Windows.Forms.Label();
            this.ScoreLabel = new System.Windows.Forms.Label();
            this.QuestionTimer = new System.Windows.Forms.Timer(this.components);
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.Red;
            this.ExitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitButton.ForeColor = System.Drawing.Color.Aqua;
            this.ExitButton.Location = new System.Drawing.Point(697, 13);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 29);
            this.ExitButton.TabIndex = 0;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.BackColor = System.Drawing.Color.Orange;
            this.UserNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.UserNameLabel.ForeColor = System.Drawing.Color.Black;
            this.UserNameLabel.Location = new System.Drawing.Point(156, 87);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(97, 24);
            this.UserNameLabel.TabIndex = 1;
            this.UserNameLabel.Text = "user name";
            // 
            // RoomNameLabel
            // 
            this.RoomNameLabel.BackColor = System.Drawing.Color.LawnGreen;
            this.RoomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RoomNameLabel.ForeColor = System.Drawing.Color.Black;
            this.RoomNameLabel.Location = new System.Drawing.Point(416, 87);
            this.RoomNameLabel.Name = "RoomNameLabel";
            this.RoomNameLabel.Size = new System.Drawing.Size(97, 24);
            this.RoomNameLabel.TabIndex = 2;
            this.RoomNameLabel.Text = "room name";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.5F);
            this.label1.ForeColor = System.Drawing.Color.Indigo;
            this.label1.Location = new System.Drawing.Point(153, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(489, 43);
            this.label1.TabIndex = 3;
            this.label1.Text = "The Hardest Cyber Trivia Challenge";
            // 
            // Answer1Button
            // 
            this.Answer1Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Answer1Button.Location = new System.Drawing.Point(103, 213);
            this.Answer1Button.Name = "Answer1Button";
            this.Answer1Button.Size = new System.Drawing.Size(140, 30);
            this.Answer1Button.TabIndex = 4;
            this.Answer1Button.Text = "Answer1Button";
            this.Answer1Button.UseVisualStyleBackColor = true;
            this.Answer1Button.Click += new System.EventHandler(this.Answer1Button_Click);
            // 
            // Answer2Button
            // 
            this.Answer2Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Answer2Button.Location = new System.Drawing.Point(555, 213);
            this.Answer2Button.Name = "Answer2Button";
            this.Answer2Button.Size = new System.Drawing.Size(140, 30);
            this.Answer2Button.TabIndex = 5;
            this.Answer2Button.Text = "Answer2Button";
            this.Answer2Button.UseVisualStyleBackColor = true;
            this.Answer2Button.Click += new System.EventHandler(this.Answer2Button_Click);
            // 
            // Answer3Button
            // 
            this.Answer3Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Answer3Button.Location = new System.Drawing.Point(103, 353);
            this.Answer3Button.Name = "Answer3Button";
            this.Answer3Button.Size = new System.Drawing.Size(140, 30);
            this.Answer3Button.TabIndex = 6;
            this.Answer3Button.Text = "Answer3Button";
            this.Answer3Button.UseVisualStyleBackColor = true;
            this.Answer3Button.Click += new System.EventHandler(this.Answer3Button_Click);
            // 
            // Answer4Button
            // 
            this.Answer4Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Answer4Button.Location = new System.Drawing.Point(555, 353);
            this.Answer4Button.Name = "Answer4Button";
            this.Answer4Button.Size = new System.Drawing.Size(140, 30);
            this.Answer4Button.TabIndex = 7;
            this.Answer4Button.Text = "Answer4Button";
            this.Answer4Button.UseVisualStyleBackColor = true;
            this.Answer4Button.Click += new System.EventHandler(this.Answer4Button_Click);
            // 
            // NumOfQuestionsLabel
            // 
            this.NumOfQuestionsLabel.AutoSize = true;
            this.NumOfQuestionsLabel.BackColor = System.Drawing.Color.Yellow;
            this.NumOfQuestionsLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11F);
            this.NumOfQuestionsLabel.Location = new System.Drawing.Point(183, 169);
            this.NumOfQuestionsLabel.Name = "NumOfQuestionsLabel";
            this.NumOfQuestionsLabel.Size = new System.Drawing.Size(103, 17);
            this.NumOfQuestionsLabel.TabIndex = 9;
            this.NumOfQuestionsLabel.Text = "questionNum";
            // 
            // QuestionTimerLable
            // 
            this.QuestionTimerLable.AutoSize = true;
            this.QuestionTimerLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.QuestionTimerLable.Location = new System.Drawing.Point(392, 279);
            this.QuestionTimerLable.Name = "QuestionTimerLable";
            this.QuestionTimerLable.Size = new System.Drawing.Size(78, 25);
            this.QuestionTimerLable.TabIndex = 10;
            this.QuestionTimerLable.Text = "TIMER";
            // 
            // ScoreLabel
            // 
            this.ScoreLabel.AutoSize = true;
            this.ScoreLabel.BackColor = System.Drawing.Color.DarkViolet;
            this.ScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.ScoreLabel.ForeColor = System.Drawing.Color.Yellow;
            this.ScoreLabel.Location = new System.Drawing.Point(326, 406);
            this.ScoreLabel.Name = "ScoreLabel";
            this.ScoreLabel.Size = new System.Drawing.Size(72, 18);
            this.ScoreLabel.TabIndex = 11;
            this.ScoreLabel.Text = "SCORE:";
            // 
            // QuestionTimer
            // 
            this.QuestionTimer.Interval = 1000;
            this.QuestionTimer.Tick += new System.EventHandler(this.QuestionTimer_Tick);
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.AutoSize = true;
            this.QuestionLabel.BackColor = System.Drawing.Color.Blue;
            this.QuestionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionLabel.ForeColor = System.Drawing.Color.Aqua;
            this.QuestionLabel.Location = new System.Drawing.Point(242, 135);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(112, 20);
            this.QuestionLabel.TabIndex = 8;
            this.QuestionLabel.Text = "currQuestion";
            // 
            // triviaGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.question_marks;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 475);
            this.Controls.Add(this.ScoreLabel);
            this.Controls.Add(this.QuestionTimerLable);
            this.Controls.Add(this.NumOfQuestionsLabel);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.Answer4Button);
            this.Controls.Add(this.Answer3Button);
            this.Controls.Add(this.Answer2Button);
            this.Controls.Add(this.Answer1Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RoomNameLabel);
            this.Controls.Add(this.UserNameLabel);
            this.Controls.Add(this.ExitButton);
            this.Name = "triviaGame";
            this.Text = "Trivia Game";
            this.Load += new System.EventHandler(this.triviaGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.Label RoomNameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Answer1Button;
        private System.Windows.Forms.Button Answer2Button;
        private System.Windows.Forms.Button Answer3Button;
        private System.Windows.Forms.Button Answer4Button;
        private System.Windows.Forms.Label NumOfQuestionsLabel;
        private System.Windows.Forms.Label QuestionTimerLable;
        private System.Windows.Forms.Label ScoreLabel;
        private System.Windows.Forms.Timer QuestionTimer;
        private System.Windows.Forms.Label QuestionLabel;
    }
}