﻿namespace loadScrean
{
    partial class myStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumOfGamesLabel = new System.Windows.Forms.Label();
            this.NumOfRightAnsLabel = new System.Windows.Forms.Label();
            this.NumOfWrongAnsLabel = new System.Windows.Forms.Label();
            this.AvgTimeForAnsLabel = new System.Windows.Forms.Label();
            this.UserLabel = new System.Windows.Forms.Label();
            this.GoBackButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NumOfGamesLabel
            // 
            this.NumOfGamesLabel.AutoSize = true;
            this.NumOfGamesLabel.BackColor = System.Drawing.Color.BlueViolet;
            this.NumOfGamesLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.NumOfGamesLabel.ForeColor = System.Drawing.Color.Yellow;
            this.NumOfGamesLabel.Location = new System.Drawing.Point(55, 120);
            this.NumOfGamesLabel.Name = "NumOfGamesLabel";
            this.NumOfGamesLabel.Size = new System.Drawing.Size(54, 19);
            this.NumOfGamesLabel.TabIndex = 0;
            this.NumOfGamesLabel.Text = "label1";
            // 
            // NumOfRightAnsLabel
            // 
            this.NumOfRightAnsLabel.AutoSize = true;
            this.NumOfRightAnsLabel.BackColor = System.Drawing.Color.Crimson;
            this.NumOfRightAnsLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.NumOfRightAnsLabel.ForeColor = System.Drawing.Color.Yellow;
            this.NumOfRightAnsLabel.Location = new System.Drawing.Point(55, 170);
            this.NumOfRightAnsLabel.Name = "NumOfRightAnsLabel";
            this.NumOfRightAnsLabel.Size = new System.Drawing.Size(54, 19);
            this.NumOfRightAnsLabel.TabIndex = 1;
            this.NumOfRightAnsLabel.Text = "label2";
            // 
            // NumOfWrongAnsLabel
            // 
            this.NumOfWrongAnsLabel.AutoSize = true;
            this.NumOfWrongAnsLabel.BackColor = System.Drawing.Color.RoyalBlue;
            this.NumOfWrongAnsLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.NumOfWrongAnsLabel.ForeColor = System.Drawing.Color.Yellow;
            this.NumOfWrongAnsLabel.Location = new System.Drawing.Point(55, 220);
            this.NumOfWrongAnsLabel.Name = "NumOfWrongAnsLabel";
            this.NumOfWrongAnsLabel.Size = new System.Drawing.Size(54, 19);
            this.NumOfWrongAnsLabel.TabIndex = 2;
            this.NumOfWrongAnsLabel.Text = "label3";
            // 
            // AvgTimeForAnsLabel
            // 
            this.AvgTimeForAnsLabel.AutoSize = true;
            this.AvgTimeForAnsLabel.BackColor = System.Drawing.Color.ForestGreen;
            this.AvgTimeForAnsLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.AvgTimeForAnsLabel.ForeColor = System.Drawing.Color.Yellow;
            this.AvgTimeForAnsLabel.Location = new System.Drawing.Point(55, 270);
            this.AvgTimeForAnsLabel.Name = "AvgTimeForAnsLabel";
            this.AvgTimeForAnsLabel.Size = new System.Drawing.Size(54, 19);
            this.AvgTimeForAnsLabel.TabIndex = 3;
            this.AvgTimeForAnsLabel.Text = "label4";
            // 
            // UserLabel
            // 
            this.UserLabel.AutoSize = true;
            this.UserLabel.BackColor = System.Drawing.Color.Aqua;
            this.UserLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.UserLabel.ForeColor = System.Drawing.Color.Blue;
            this.UserLabel.Location = new System.Drawing.Point(68, 49);
            this.UserLabel.Name = "UserLabel";
            this.UserLabel.Size = new System.Drawing.Size(66, 24);
            this.UserLabel.TabIndex = 4;
            this.UserLabel.Text = "label5";
            // 
            // GoBackButton
            // 
            this.GoBackButton.BackColor = System.Drawing.Color.Red;
            this.GoBackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GoBackButton.ForeColor = System.Drawing.Color.Yellow;
            this.GoBackButton.Location = new System.Drawing.Point(460, 12);
            this.GoBackButton.Name = "GoBackButton";
            this.GoBackButton.Size = new System.Drawing.Size(75, 25);
            this.GoBackButton.TabIndex = 5;
            this.GoBackButton.Text = "Go Back";
            this.GoBackButton.UseVisualStyleBackColor = false;
            this.GoBackButton.Click += new System.EventHandler(this.GoBackButton_Click);
            // 
            // myStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.status_picture;
            this.ClientSize = new System.Drawing.Size(547, 338);
            this.Controls.Add(this.GoBackButton);
            this.Controls.Add(this.UserLabel);
            this.Controls.Add(this.AvgTimeForAnsLabel);
            this.Controls.Add(this.NumOfWrongAnsLabel);
            this.Controls.Add(this.NumOfRightAnsLabel);
            this.Controls.Add(this.NumOfGamesLabel);
            this.Name = "myStatus";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "myStatus";
            this.Load += new System.EventHandler(this.myStatus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NumOfGamesLabel;
        private System.Windows.Forms.Label NumOfRightAnsLabel;
        private System.Windows.Forms.Label NumOfWrongAnsLabel;
        private System.Windows.Forms.Label AvgTimeForAnsLabel;
        private System.Windows.Forms.Label UserLabel;
        private System.Windows.Forms.Button GoBackButton;
    }
}