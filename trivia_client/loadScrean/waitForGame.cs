﻿using System;
using System.Net.Sockets;
using System.Windows.Forms;

using System.Threading;

using System.Drawing;
using System.Text;

namespace loadScrean
{
    public partial class waitForGame : Form
    {
        private bool _isAdmin;
        private string _roomName;
        private int _maxNumOfPlayers;
        private int _numOfQuestions;
        private int _timePerQuestion;
        private string _username;
        private bool _isGameStart;

        private bool _closeThread;
        private Thread _leason_thread;

        private NetworkStream _clientStream;
        private byte[] _bufferGet;
        private byte[] _bufferSet;


        public waitForGame(NetworkStream clientStream, string username, bool isAdmin, 
                           string roomName, string maxNumOfPlayers, string numOfQuestions, string timePerQuestion)
        {
            _clientStream = clientStream;
            _username = username;
            _isAdmin = isAdmin;
            _roomName = roomName;
            _maxNumOfPlayers = Convert.ToInt32(maxNumOfPlayers);
            _numOfQuestions = Convert.ToInt32(numOfQuestions);
            _timePerQuestion = Convert.ToInt32(timePerQuestion);
            _isGameStart = false;

            _closeThread = false;
            _leason_thread = new Thread(leason);

            InitializeComponent();

            // init the window.
            userNameLable.Text = _username;
            roomNameLabel.Text = _roomName;

            number_of_questionsLabel.Text += _numOfQuestions;
            nax_number_playersLabel.Text += _maxNumOfPlayers;
            time_per_questionLabel.Text += _timePerQuestion;

            connactionInfoLabel.Text = "You are connected to the room: " + _roomName;
        }

        private void leason()
        {
            while (true)
            {
                if ((_closeThread) || (_isGameStart))
                {
                    return;
                }
                else
                {
                    int bytesRead;
                    string typeMessage;
                    try
                    {
                        // get the type of the message
                        _bufferGet = new byte[3];
                        bytesRead = _clientStream.Read(_bufferGet, 0, 3);
                        typeMessage = new ASCIIEncoding().GetString(_bufferGet);

                        // -----check the typeMessage.----
                        // get update of players in the room.
                        if (typeMessage == "108")
                        {
                            // refresh the panel (because maby any player leave...)
                            Invoke((MethodInvoker)delegate { playersInRoomPanel.Controls.Clear(); });

                            _bufferGet = new byte[1];
                            bytesRead = _clientStream.Read(_bufferGet, 0, 1);
                            string numOfPlayers = new ASCIIEncoding().GetString(_bufferGet);

                            // get all the players in the room.
                            for (int i = Convert.ToInt32(numOfPlayers); i > 0; i--)
                            {
                                // get the length of playerName
                                _bufferGet = new byte[2];
                                bytesRead = _clientStream.Read(_bufferGet, 0, 2);
                                string playerNameLen = new ASCIIEncoding().GetString(_bufferGet);

                                // get the name of player
                                _bufferGet = new byte[Convert.ToInt32(playerNameLen)];
                                bytesRead = _clientStream.Read(_bufferGet, 0, Convert.ToInt32(playerNameLen));
                                string playerName = new ASCIIEncoding().GetString(_bufferGet);

                                // add the player to the panel
                                Label playerNameLabel = new Label();
                                playerNameLabel.Text = playerName;
                                playerNameLabel.Location = new Point(5, playersInRoomPanel.Controls.Count * 30);
                                playerNameLabel.AutoSize = true;
                                playerNameLabel.Font = new Font("Monotype Hadassah", 20);
                                Invoke((MethodInvoker)delegate { playersInRoomPanel.Controls.Add(playerNameLabel); });
                            }
                        }

                        // the room is closed.
                        else if (typeMessage == "116")
                        {
                            if (_isAdmin) // if the user is the admin (that closed the room!).
                            {
                                Invoke((MethodInvoker)delegate { this.Hide(); });
                                Invoke((MethodInvoker)delegate { this.Close(); });
                            }
                            else // if the user is not the admin.
                            {
                                Invoke((MethodInvoker)delegate { leaveRoomButton.Visible = false; });
                                Invoke((MethodInvoker)delegate { okButton.Visible = true; });
                                Invoke((MethodInvoker)delegate { errorLabel.Visible = true; });
                                Invoke((MethodInvoker)delegate { errorLabel.Text = "the room is closed by admin, click ok to continue."; });
                            }
                            break; // have to stop the thread, if not is continue to leason....
                        }
                        else if(typeMessage == "118")
                        {
                            _isGameStart = true;

                            Invoke((MethodInvoker)delegate
                            {
                                triviaGame newTriviaGame_window = new triviaGame(_clientStream, _username, _roomName,
                                                                                 _numOfQuestions.ToString(), _timePerQuestion.ToString());

                                this.Hide();

                                try
                                {
                                    newTriviaGame_window.ShowDialog();
                                }
                                catch (Exception) { }

                                this.Close();
                            });
                        }

                    }
                    catch (Exception) { };
                }
            }
        }


        private void waitForGame_Load(object sender, EventArgs e)
        {
            if (_isAdmin == true) // the user is the admin.
            {
                leaveRoomButton.Visible = false;

                // add the admin to the panel
                Label playerNameLabel = new Label();
                playerNameLabel.Text = _username;
                playerNameLabel.Location = new Point(5, playersInRoomPanel.Controls.Count * 30);
                playerNameLabel.AutoSize = true;
                playerNameLabel.Font = new Font("Monotype Hadassah", 20);
                playersInRoomPanel.Controls.Add(playerNameLabel);

                // start to leason to messages from the server
                _leason_thread.Start();
            }
            else  // the user is not the admin
            {
                // unvisible the label of the admin.
                nax_number_playersLabel.Visible = false;

                // unvisible the button of the admin.
                closeRoomButton.Visible = false;
                startGameButton.Visible = false;


                // start to leason to messages from the server
                _leason_thread.Start();
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void closeRoomButton_Click(object sender, EventArgs e)
        {
            _closeThread = true; // the thread have to close!

            // send the 215 message of close room.
            _bufferSet = new ASCIIEncoding().GetBytes("215");
            _clientStream.Write(_bufferSet, 0, _bufferSet.Length);
            _clientStream.Flush();
        }

        private void leaveRoomButton_Click(object sender, EventArgs e)
        {
            _closeThread = true; // the thread have to close!

            // send the 211 message of leave room.
            _bufferSet = new ASCIIEncoding().GetBytes("211");
            _clientStream.Write(_bufferSet, 0, _bufferSet.Length);
            _clientStream.Flush();

            // get the answer of sucssess.
            _bufferGet = new byte[4];
            int bytesRead = _clientStream.Read(_bufferGet, 0, 4);
            string typeOfMessage = new ASCIIEncoding().GetString(_bufferGet);

            this.Hide();
            this.Close();
        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            // send the 217 message to start the game
            _bufferSet = new ASCIIEncoding().GetBytes("217");
            _clientStream.Write(_bufferSet, 0, _bufferSet.Length);
            _clientStream.Flush();
        }
    }
}
