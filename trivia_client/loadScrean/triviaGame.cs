﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class triviaGame : Form
    {
        private string _userName;
        private string _roomName;
        private string _numOfQuestions;
        private string _timePerQuestion;
        private string _msgCode;

        private Thread _leasten_thread;
        private NetworkStream _clientStream;
        private byte[] bufferGet;
        private byte[] bufferSet;

        private bool _isExit;
        private bool _takeMsgCode;

        Color[] colors = new Color[] {Color.DarkBlue, Color.DarkCyan, Color.DarkGoldenrod, Color.DarkGreen, Color.DarkKhaki, Color.DarkMagenta,
                                      Color.DarkOliveGreen, Color.DarkOrange, Color.DarkOrchid, Color.DarkRed, Color.DarkSalmon, Color.DarkSeaGreen,
                                      Color.DarkSlateBlue, Color.DarkSlateGray, Color.DarkTurquoise, Color.DarkViolet};

        System.Media.SoundPlayer triviaGame_music = new System.Media.SoundPlayer(loadScrean.Properties.Resources.triviaGame_music);
        System.Media.SoundPlayer rightAns_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.good_answer_sound);
        System.Media.SoundPlayer wrongAns_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.bad_answer_sound);

        public triviaGame(NetworkStream clientStream, string username, string roomName, string numOfQuestions, string timePerQuestion)
        {
            // initializing fields
            _clientStream = clientStream;
            _userName = username;
            _roomName = roomName;
            _numOfQuestions = numOfQuestions;
            _timePerQuestion = timePerQuestion;

            _msgCode = "118";
            _isExit = false;
            _takeMsgCode = false;

            _leasten_thread = new Thread(StartTriviaGame);

            InitializeComponent();

            // initializing lables
            UserNameLabel.Text = _userName;
            RoomNameLabel.Text = _roomName;
        }

        private void triviaGame_Load(object sender, EventArgs e)
        {
            _leasten_thread.Start();

        }

        private void StartTriviaGame()
        {
            bool first_loop = true; // this bool, is to stop the loop if player clicked on exit.

            int currQuestionNum = 1;
            int userScore = 0;
            Invoke((MethodInvoker)delegate { ScoreLabel.Text = userScore.ToString(); });

            while (true)
            {
                triviaGame_music.PlayLooping(); // start the trivia game music and loop
                try
                {
                    // this while and if, is to stop the loop if player clicked on exit.
                    if (!first_loop)
                    {
                        while (!_takeMsgCode)
                        {
                            if (_isExit)
                                break;
                        }
                        if (_isExit)
                            break;
                    }
                    first_loop = false;

                    
                    if (_takeMsgCode)
                    {
                        // קבלת סוג ההודעה מהסרבר
                        bufferGet = new byte[3];
                        int bytesRead = _clientStream.Read(bufferGet, 0, 3); // !!!
                        _msgCode = new ASCIIEncoding().GetString(bufferGet);
                    }

                    _takeMsgCode = false; // restart, because just the button can allow it.

                    if (_msgCode == "118")
                    {
                        bufferGet = new byte[3];
                        int bytesRead2 = _clientStream.Read(bufferGet, 0, 3);
                        string questionLen = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[Convert.ToInt32(questionLen)];
                        int bytesRead3 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(questionLen));
                        string question = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[3];
                        int bytesRead4 = _clientStream.Read(bufferGet, 0, 3);
                        string ans1Len = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[Convert.ToInt32(ans1Len)];
                        int bytesRead5 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans1Len));
                        string ans1 = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[3];
                        int bytesRead6 = _clientStream.Read(bufferGet, 0, 3);
                        string ans2Len = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[Convert.ToInt32(ans2Len)];
                        int bytesRead7 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans2Len));
                        string ans2 = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[3];
                        int bytesRead8 = _clientStream.Read(bufferGet, 0, 3);
                        string ans3Len = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[Convert.ToInt32(ans3Len)];
                        int bytesRead9 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans3Len));
                        string ans3 = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[3];
                        int bytesRead10 = _clientStream.Read(bufferGet, 0, 3);
                        string ans4Len = new ASCIIEncoding().GetString(bufferGet);

                        bufferGet = new byte[Convert.ToInt32(ans4Len)];
                        int bytesRead11 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans4Len));
                        string ans4 = new ASCIIEncoding().GetString(bufferGet);

                        // לקיחת המידע מהסרבר והשמתו בלייבלים ובכפתורים המתאימים
                        Invoke((MethodInvoker)delegate
                        {
                            QuestionLabel.Text = question; // set the question that was sent by the server
                            NumOfQuestionsLabel.Text = "Question: " + currQuestionNum.ToString() + "/" + _numOfQuestions;
                            ScoreLabel.Text = "Your score: " + userScore.ToString();
                        });

                        currQuestionNum++;

                        Invoke((MethodInvoker)delegate
                        {
                            // set the answers thet was sent by the server
                            Answer1Button.Text = ans1;
                            Answer2Button.Text = ans2;
                            Answer3Button.Text = ans3;
                            Answer4Button.Text = ans4;

                            Random rnd = new Random();
                            int colorIndex = rnd.Next(colors.Length - 1);
                            Answer1Button.ForeColor = colors[colorIndex];

                            colorIndex = rnd.Next(colors.Length - 1);
                            Answer2Button.ForeColor = colors[colorIndex];

                            colorIndex = rnd.Next(colors.Length - 1);
                            Answer3Button.ForeColor = colors[colorIndex];

                            colorIndex = rnd.Next(colors.Length - 1);
                            Answer4Button.ForeColor = colors[colorIndex];

                            QuestionTimerLable.Text = _timePerQuestion; // seting the starting timer value
                            QuestionTimer.Start(); // start the timer
                        });

                    }

                    else if(_msgCode == "120")
                    {
                        // קבלת תשובה מהיוזר ושליחתה לסרבר
                        // בדיקה על איזה כפתור היוזר לחץ

                        bufferGet = new byte[1];
                        int bytesRead13 = _clientStream.Read(bufferGet, 0, 1);
                        string isAnsRight = new ASCIIEncoding().GetString(bufferGet);

                        // העלאת נקודות על תשובה נכונה
                        if (isAnsRight == "1") // the answer is right
                        {
                            userScore++;
                            Invoke((MethodInvoker)delegate { ScoreLabel.Text = "Your Score: " + userScore.ToString(); });
                            rightAns_sound.Play(); // make a good answer sound
                            System.Threading.Thread.Sleep(2000);
                        }
                        else // the answer is wrong
                        {
                            wrongAns_sound.Play(); // make a bad answer sound
                            System.Threading.Thread.Sleep(2000);
                        }

                        _takeMsgCode = true;
                    }
                    else if (_msgCode == "121")
                    {
                        // סיום משחק
                        triviaGame_music.Stop();

                        // get from the server the number of users ther was in the game
                        bufferGet = new byte[1];
                        int bytesRead15 = _clientStream.Read(bufferGet, 0, 1);
                        string numOfUsers = new ASCIIEncoding().GetString(bufferGet);

                        List<string> usersNamesAndScores = new List<string>();

                        for (int j = 0; j < Convert.ToInt32(numOfUsers); j++)
                        {
                            bufferGet = new byte[2];
                            int bytesRead16 = _clientStream.Read(bufferGet, 0, 2);
                            string userNameLen = new ASCIIEncoding().GetString(bufferGet);

                            bufferGet = new byte[Convert.ToInt32(userNameLen)];
                            int bytesRead17 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(userNameLen));
                            string userName = new ASCIIEncoding().GetString(bufferGet);

                            usersNamesAndScores.Add(userName); // add the user name to the list 

                            bufferGet = new byte[2];
                            int bytesRead18 = _clientStream.Read(bufferGet, 0, 2);
                            string userScoreFromServer = new ASCIIEncoding().GetString(bufferGet);

                            usersNamesAndScores.Add(userScoreFromServer); // add the user score to the list
                        }

                        Invoke((MethodInvoker)delegate
                        {
                            Scores scores_window = new Scores(usersNamesAndScores);

                            try
                            {
                                scores_window.ShowDialog();
                            }
                            catch (Exception) { }

                            this.Close();
                        });
                        break;
                    }
                
                }
                catch (Exception) { }
            }
        }

        private void Answer1Button_Click(object sender, EventArgs e)
        {
            QuestionTimer.Stop();

            string ansMsgCode = "219";
            string ansIndex = "1";
            string userAnswerTime = ((Convert.ToInt32(_timePerQuestion)) - (Convert.ToInt32(QuestionTimerLable.Text))).ToString();
            int userAnsweTimeInt = Convert.ToInt32(userAnswerTime);
            userAnswerTime = userAnsweTimeInt.ToString("D2");
            string fullMsgToServer = ansMsgCode + ansIndex + userAnswerTime;

            // sending to the server the question answer code (219), 
            // the index of the answer the user chose and the time thet took the user to do so 
            bufferSet = new ASCIIEncoding().GetBytes(fullMsgToServer);
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

            _takeMsgCode = true; //allow the thread to get message from server.
        }

        private void Answer2Button_Click(object sender, EventArgs e)
        {
            QuestionTimer.Stop();

            string ansMsgCode = "219";
            string ansIndex = "2";
            string userAnswerTime = ((Convert.ToInt32(_timePerQuestion)) - (Convert.ToInt32(QuestionTimerLable.Text))).ToString();
            int userAnsweTimeInt = Convert.ToInt32(userAnswerTime);
            userAnswerTime = userAnsweTimeInt.ToString("D2");
            string fullMsgToServer = ansMsgCode + ansIndex + userAnswerTime;

            // sending to the server the question answer code (219), 
            // the index of the answer the user chose and the time thet took the user to do so 
            bufferSet = new ASCIIEncoding().GetBytes(fullMsgToServer);
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

            _takeMsgCode = true; //allow the thread to get message from server.
        }

        private void Answer3Button_Click(object sender, EventArgs e)
        {
            QuestionTimer.Stop();

            string ansMsgCode = "219";
            string ansIndex = "3";
            string userAnswerTime = ((Convert.ToInt32(_timePerQuestion)) - (Convert.ToInt32(QuestionTimerLable.Text))).ToString();
            int userAnsweTimeInt = Convert.ToInt32(userAnswerTime);
            userAnswerTime = userAnsweTimeInt.ToString("D2");
            string fullMsgToServer = ansMsgCode + ansIndex + userAnswerTime;

            // sending to the server the question answer code (219), 
            // the index of the answer the user chose and the time thet took the user to do so 
            bufferSet = new ASCIIEncoding().GetBytes(fullMsgToServer);
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

            _takeMsgCode = true; //allow the thread to get message from server.
        }

        private void Answer4Button_Click(object sender, EventArgs e)
        {
            QuestionTimer.Stop();

            string ansMsgCode = "219";
            string ansIndex = "4";
            string userAnswerTime = ((Convert.ToInt32(_timePerQuestion)) - (Convert.ToInt32(QuestionTimerLable.Text))).ToString();
            int userAnsweTimeInt = Convert.ToInt32(userAnswerTime);
            userAnswerTime = userAnsweTimeInt.ToString("D2");
            string fullMsgToServer = ansMsgCode + ansIndex + userAnswerTime;

            // sending to the server the question answer code (219), 
            // the index of the answer the user chose and the time thet took the user to do so 
            bufferSet = new ASCIIEncoding().GetBytes(fullMsgToServer);
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

            _takeMsgCode = true; //allow the thread to get message from server.
        }
        
        private void ExitButton_Click(object sender, EventArgs e)
        {
            _isExit = true;

            // send to the server an exit code message (222)
            bufferSet = new ASCIIEncoding().GetBytes("222");
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

             // need to close the tread?
             this.Hide();
             this.Close();
        }

        private void QuestionTimer_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int colorIndex = rnd.Next(colors.Length - 1);
            QuestionTimerLable.ForeColor = colors[colorIndex];

            int timerValue = Convert.ToInt32(QuestionTimerLable.Text);
            timerValue--; // dercrease the timer value by 1 each second
            QuestionTimerLable.Text = timerValue.ToString();

            if (QuestionTimerLable.Text == "0") // means thet the time is over
            {
                QuestionTimer.Stop();
                string ansIndex = "5";
                string ansMsgCode = "219";
                string userAnswerTime = Convert.ToInt32(_timePerQuestion).ToString("D2");

                string fullMsgToServer = ansMsgCode + ansIndex + userAnswerTime;

                // sending to the server the question answer code (219), 
                // the index of the answer the user chose and the time thet took the user to do so 
                bufferSet = new ASCIIEncoding().GetBytes(fullMsgToServer);
                _clientStream.Write(bufferSet, 0, bufferSet.Length);
                _clientStream.Flush();

                _takeMsgCode = true; //allow the thread to get message from server.
            }
        }
    }
}
