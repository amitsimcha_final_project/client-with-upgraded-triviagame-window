﻿namespace loadScrean
{
    partial class waitForGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.connactionInfoLabel = new System.Windows.Forms.Label();
            this.userNameLable = new System.Windows.Forms.Label();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.closeRoomButton = new System.Windows.Forms.Button();
            this.startGameButton = new System.Windows.Forms.Button();
            this.playersInRoomPanel = new System.Windows.Forms.Panel();
            this.number_of_questionsLabel = new System.Windows.Forms.Label();
            this.nax_number_playersLabel = new System.Windows.Forms.Label();
            this.time_per_questionLabel = new System.Windows.Forms.Label();
            this.leaveRoomButton = new System.Windows.Forms.Button();
            this.errorLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(223, 223);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(543, 179);
            this.label1.TabIndex = 0;
            // 
            // connactionInfoLabel
            // 
            this.connactionInfoLabel.AutoEllipsis = true;
            this.connactionInfoLabel.AutoSize = true;
            this.connactionInfoLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.connactionInfoLabel.Font = new System.Drawing.Font("Segoe Script", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connactionInfoLabel.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.connactionInfoLabel.Location = new System.Drawing.Point(257, 162);
            this.connactionInfoLabel.Name = "connactionInfoLabel";
            this.connactionInfoLabel.Size = new System.Drawing.Size(352, 34);
            this.connactionInfoLabel.TabIndex = 1;
            this.connactionInfoLabel.Text = "you are connected to the room";
            this.connactionInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // userNameLable
            // 
            this.userNameLable.AutoSize = true;
            this.userNameLable.BackColor = System.Drawing.Color.White;
            this.userNameLable.Font = new System.Drawing.Font("Ink Free", 25F, System.Drawing.FontStyle.Bold);
            this.userNameLable.Location = new System.Drawing.Point(21, 24);
            this.userNameLable.Name = "userNameLable";
            this.userNameLable.Size = new System.Drawing.Size(253, 42);
            this.userNameLable.TabIndex = 2;
            this.userNameLable.Text = "userNameLable";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.BackColor = System.Drawing.Color.White;
            this.roomNameLabel.Font = new System.Drawing.Font("Ink Free", 25F, System.Drawing.FontStyle.Bold);
            this.roomNameLabel.Location = new System.Drawing.Point(461, 24);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(260, 42);
            this.roomNameLabel.TabIndex = 3;
            this.roomNameLabel.Text = "roomNameLable";
            // 
            // closeRoomButton
            // 
            this.closeRoomButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.closeRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeRoomButton.Font = new System.Drawing.Font("Ravie", 15F);
            this.closeRoomButton.ForeColor = System.Drawing.Color.White;
            this.closeRoomButton.Location = new System.Drawing.Point(24, 306);
            this.closeRoomButton.Name = "closeRoomButton";
            this.closeRoomButton.Size = new System.Drawing.Size(189, 58);
            this.closeRoomButton.TabIndex = 4;
            this.closeRoomButton.Text = "close room";
            this.closeRoomButton.UseVisualStyleBackColor = false;
            this.closeRoomButton.Click += new System.EventHandler(this.closeRoomButton_Click);
            // 
            // startGameButton
            // 
            this.startGameButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.startGameButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startGameButton.Font = new System.Drawing.Font("Ravie", 15F);
            this.startGameButton.ForeColor = System.Drawing.Color.White;
            this.startGameButton.Location = new System.Drawing.Point(24, 235);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(193, 57);
            this.startGameButton.TabIndex = 5;
            this.startGameButton.Text = "start game";
            this.startGameButton.UseVisualStyleBackColor = false;
            this.startGameButton.Click += new System.EventHandler(this.startGameButton_Click);
            // 
            // playersInRoomPanel
            // 
            this.playersInRoomPanel.AutoScroll = true;
            this.playersInRoomPanel.BackColor = System.Drawing.Color.White;
            this.playersInRoomPanel.Location = new System.Drawing.Point(337, 264);
            this.playersInRoomPanel.Name = "playersInRoomPanel";
            this.playersInRoomPanel.Size = new System.Drawing.Size(338, 100);
            this.playersInRoomPanel.TabIndex = 6;
            // 
            // number_of_questionsLabel
            // 
            this.number_of_questionsLabel.AutoSize = true;
            this.number_of_questionsLabel.BackColor = System.Drawing.Color.Black;
            this.number_of_questionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.number_of_questionsLabel.ForeColor = System.Drawing.Color.White;
            this.number_of_questionsLabel.Location = new System.Drawing.Point(236, 235);
            this.number_of_questionsLabel.Name = "number_of_questionsLabel";
            this.number_of_questionsLabel.Size = new System.Drawing.Size(172, 20);
            this.number_of_questionsLabel.TabIndex = 7;
            this.number_of_questionsLabel.Text = "number_of_questions: ";
            // 
            // nax_number_playersLabel
            // 
            this.nax_number_playersLabel.AutoSize = true;
            this.nax_number_playersLabel.BackColor = System.Drawing.Color.Black;
            this.nax_number_playersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.nax_number_playersLabel.ForeColor = System.Drawing.Color.White;
            this.nax_number_playersLabel.Location = new System.Drawing.Point(414, 235);
            this.nax_number_playersLabel.Name = "nax_number_playersLabel";
            this.nax_number_playersLabel.Size = new System.Drawing.Size(168, 20);
            this.nax_number_playersLabel.TabIndex = 8;
            this.nax_number_playersLabel.Text = "max_number_players: ";
            // 
            // time_per_questionLabel
            // 
            this.time_per_questionLabel.AutoSize = true;
            this.time_per_questionLabel.BackColor = System.Drawing.Color.Black;
            this.time_per_questionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.time_per_questionLabel.ForeColor = System.Drawing.Color.White;
            this.time_per_questionLabel.Location = new System.Drawing.Point(588, 235);
            this.time_per_questionLabel.Name = "time_per_questionLabel";
            this.time_per_questionLabel.Size = new System.Drawing.Size(149, 20);
            this.time_per_questionLabel.TabIndex = 9;
            this.time_per_questionLabel.Text = "time_per_question: ";
            // 
            // leaveRoomButton
            // 
            this.leaveRoomButton.BackColor = System.Drawing.Color.Black;
            this.leaveRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.leaveRoomButton.Font = new System.Drawing.Font("Ravie", 20.25F);
            this.leaveRoomButton.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.leaveRoomButton.Location = new System.Drawing.Point(240, 489);
            this.leaveRoomButton.Name = "leaveRoomButton";
            this.leaveRoomButton.Size = new System.Drawing.Size(526, 53);
            this.leaveRoomButton.TabIndex = 10;
            this.leaveRoomButton.Text = "leave room";
            this.leaveRoomButton.UseVisualStyleBackColor = false;
            this.leaveRoomButton.Click += new System.EventHandler(this.leaveRoomButton_Click);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.BackColor = System.Drawing.Color.White;
            this.errorLabel.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(278, 367);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(63, 20);
            this.errorLabel.TabIndex = 11;
            this.errorLabel.Text = "error";
            this.errorLabel.Visible = false;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.okButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.okButton.Font = new System.Drawing.Font("Ravie", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.ForeColor = System.Drawing.Color.White;
            this.okButton.Location = new System.Drawing.Point(429, 405);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(147, 53);
            this.okButton.TabIndex = 12;
            this.okButton.Text = "ok";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Visible = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // waitForGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.שעונים_מעוררים;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(779, 554);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.leaveRoomButton);
            this.Controls.Add(this.time_per_questionLabel);
            this.Controls.Add(this.nax_number_playersLabel);
            this.Controls.Add(this.number_of_questionsLabel);
            this.Controls.Add(this.playersInRoomPanel);
            this.Controls.Add(this.startGameButton);
            this.Controls.Add(this.closeRoomButton);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.userNameLable);
            this.Controls.Add(this.connactionInfoLabel);
            this.Controls.Add(this.label1);
            this.Name = "waitForGame";
            this.Text = "Wait For Game";
            this.Load += new System.EventHandler(this.waitForGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label connactionInfoLabel;
        private System.Windows.Forms.Label userNameLable;
        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Button closeRoomButton;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.Panel playersInRoomPanel;
        private System.Windows.Forms.Label number_of_questionsLabel;
        private System.Windows.Forms.Label nax_number_playersLabel;
        private System.Windows.Forms.Label time_per_questionLabel;
        private System.Windows.Forms.Button leaveRoomButton;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.Button okButton;
    }
}