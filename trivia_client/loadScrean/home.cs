﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Speech.Synthesis;

namespace loadScrean
{
    public partial class home : Form
    {
        private TcpClient client;
        private IPEndPoint serverEndPoint;
        private NetworkStream clientStream;
        private byte[] bufferGet;
        private byte[] bufferSet;
        bool isAdmin;

        private SpeechSynthesizer synthesizer = new SpeechSynthesizer();

        private string username;
        private string messageToServer;

        // init the sounds.
        System.Media.SoundPlayer angry_birds_rio_music = new System.Media.SoundPlayer(loadScrean.Properties.Resources.Angry_Birds_Rio_music);
        System.Media.SoundPlayer welcome_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.welcome_sound);
        System.Media.SoundPlayer podium_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.podium_sound);
        System.Media.SoundPlayer hammering_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.hammer_sound);
        System.Media.SoundPlayer sawing_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.saw_sound);

        public home()
        {
            bool tryConnect = true;
           
            // התחברות לשרת
            client = new TcpClient();
            serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8826);

            while (tryConnect)
            {
                try
                {
                    client.Connect(serverEndPoint);
                    clientStream = client.GetStream();

                    tryConnect = false;
                    angry_birds_rio_music.Play(); // start play the music
                    InitializeComponent();

                    // frees the buttons, but not sign in, sign up and quit.
                    joinRoomButton.Enabled = false;
                    createRoomButton.Enabled = false;
                    myStatusButton.Enabled = false;
                    bestScoresButton.Enabled = false;
                }
                catch (Exception)
                {
                    // bulis the messageBox
                    string message = "you do not have a server running.\r\rOk - try again.\rCancel - close client.";
                    MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                    string caption = "Error Detected";
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);

                    // check witch button was clicked.
                    if (result == DialogResult.Cancel)
                    {
                        tryConnect = false;
                        this.Hide();
                        this.Close();
                    }
                }
            }
        }

        // כפתור ההתנתקות
        private void quitButton_Click(object sender, EventArgs e)
        {
            bufferSet = new ASCIIEncoding().GetBytes("299");
            clientStream.Write(bufferSet, 0, bufferSet.Length);
            clientStream.Flush();

            this.Hide();
            this.Close();
        }

        // כפתור הכניסה
        private void signInButton_Click(object sender, EventArgs e)
        {
            string usernameLen = "";
            string passwordLen = "";

            //check if the textBoxes of username and pass is not empty.
            if (usernameTextBox.Text == "" || passwordTextBox.Text == "")
                errorMessageLable.Text = "?!תגיד לי בעצמך, מה חסר";
            else
            {
                // restart the erreo message.
                errorMessageLable.Text = "";

                // bulid the message
                usernameLen = usernameTextBox.Text.Length.ToString();
                if (usernameLen.Length < 2)
                    usernameLen = "0" + usernameTextBox.Text.Length;
                passwordLen = passwordTextBox.Text.Length.ToString();
                if (passwordLen.Length < 2)
                    passwordLen = "0" + passwordTextBox.Text.Length;
                messageToServer = "200" + usernameLen + usernameTextBox.Text + passwordLen + passwordTextBox.Text;

                //send sign in message "200"
                bufferSet = new ASCIIEncoding().GetBytes(messageToServer);
                clientStream.Write(bufferSet, 0, messageToServer.Length);
                clientStream.Flush();

                // get the answer from the server.
                bufferGet = new byte[4];
                int bytesRead = clientStream.Read(bufferGet, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferGet);

                if (input == "1021")
                {
                    errorMessageLable.Text = "...אופסי דופסי!! המידע שגוי";
                }
                else if (input == "1022")
                {
                    errorMessageLable.Text = "..אמממממ... אתה מחובר כבר";
                }
                else if (input == "1020")
                {
                    // dissapear all the sign in area.
                    username = usernameTextBox.Text;
                    usernameLable.Visible = false;
                    usernameTextBox.Visible = false;
                    passwordLabel.Visible = false;
                    passwordTextBox.Visible = false;
                    signInButton.Visible = false;
                    singOutButton.Visible = true;

                    // free the buttons.
                    joinRoomButton.Enabled = true;
                    createRoomButton.Enabled = true;
                    myStatusButton.Enabled = true;
                    bestScoresButton.Enabled = true;

                    // appear the name of the user name.
                    currUsernameLabel.Text = username;
                    currFrontUsernameLable.Text = username + " שלום לך";
                    currFrontUsernameLable.Visible = true;

                    welcome_sound.Play(); // play the "welcome" for the new user the connected.
                    System.Threading.Thread.Sleep(1000); // wait because the next line is stop the "welcome" sound.
                    angry_birds_rio_music.Play(); // continue the backgrownd music.
                }
                else
                    errorMessageLable.Text = input + " קוד ההודעה הוא";
            }
        }

        // כפתור היציאה
        private void singOutButton_Click(object sender, EventArgs e)
        {
            bufferSet = new ASCIIEncoding().GetBytes("201");
            clientStream.Write(bufferSet, 0, bufferSet.Length);
            clientStream.Flush();

            username = "";
            currUsernameLabel.Text = "";
            currFrontUsernameLable.Text = "";
            usernameTextBox.Text = "";
            passwordTextBox.Text = "";
            usernameLable.Visible = true;
            usernameTextBox.Visible = true;
            passwordLabel.Visible = true;
            passwordTextBox.Visible = true;
            signInButton.Visible = true;
            singOutButton.Visible = false;

            joinRoomButton.Enabled = false;
            createRoomButton.Enabled = false;
            myStatusButton.Enabled = false;
            bestScoresButton.Enabled = false;
        }

        // כפתור ההרשמה
        private void singUpButton_Click(object sender, EventArgs e)
        {
            angry_birds_rio_music.Stop(); // stop the music of the backgrownd.
            this.Hide();

            SignUp sign_up_window = new SignUp(clientStream);

            try
            {
                sign_up_window.ShowDialog();
            }
            catch (Exception) { }

            angry_birds_rio_music.Play(); // continue the music of the backgrownd.
            this.Show();
        }

        // כפתור התוצאות הגבוהות
        private void bestScoresButton_Click(object sender, EventArgs e)
        {
            // send best score message to the server (223).
            bufferSet = new ASCIIEncoding().GetBytes("223");
            clientStream.Write(bufferSet, 0, bufferSet.Length);
            clientStream.Flush();

            // get the type answer from the server.
            bufferGet = new byte[3];
            int bytesRead1 = clientStream.Read(bufferGet, 0, 3);
            string typeMessage = new ASCIIEncoding().GetString(bufferGet);

            // get the length of username place 1
            bufferGet = new byte[2];
            int bytesRead2 = clientStream.Read(bufferGet, 0, 2);
            string place1usernameLen = new ASCIIEncoding().GetString(bufferGet);

            // get the name of username place 1
            bufferGet = new byte[Convert.ToInt32(place1usernameLen)];
            int bytesRead3 = clientStream.Read(bufferGet, 0, Convert.ToInt32(place1usernameLen));
            string place1name = new ASCIIEncoding().GetString(bufferGet);

            // get the score of username place 1
            bufferGet = new byte[6];
            int bytesRead4 = clientStream.Read(bufferGet, 0, 6);
            string place1Score = new ASCIIEncoding().GetString(bufferGet);

            // get the length of username place 2
            bufferGet = new byte[2];
            int bytesRead5 = clientStream.Read(bufferGet, 0, 2);
            string place2usernameLen = new ASCIIEncoding().GetString(bufferGet);

            // get the name of username place 2
            bufferGet = new byte[Convert.ToInt32(place2usernameLen)];
            int bytesRead6 = clientStream.Read(bufferGet, 0, Convert.ToInt32(place2usernameLen));
            string place2name = new ASCIIEncoding().GetString(bufferGet);

            // get the score of username place 2
            bufferGet = new byte[6];
            int bytesRead7 = clientStream.Read(bufferGet, 0, 6);
            string place2Score = new ASCIIEncoding().GetString(bufferGet);

            // get the length of username place 3
            bufferGet = new byte[2];
            int bytesRead8 = clientStream.Read(bufferGet, 0, 2);
            string place3usernameLen = new ASCIIEncoding().GetString(bufferGet);

            // get the name of username place 3
            bufferGet = new byte[Convert.ToInt32(place3usernameLen)];
            int bytesRead9 = clientStream.Read(bufferGet, 0, Convert.ToInt32(place3usernameLen));
            string place3name = new ASCIIEncoding().GetString(bufferGet);

            // get the score of username place 3
            bufferGet = new byte[6];
            int bytesRead10 = clientStream.Read(bufferGet, 0, 6);
            string place3Score = new ASCIIEncoding().GetString(bufferGet);

            try
            {
                bestScore newBestScoreWindow = new bestScore(username, place1name, place1Score,
                                                             place2name, place2Score, place3name, place3Score);

                podium_sound.Play(); // play the podium sound.
                this.Hide();
            
                newBestScoreWindow.ShowDialog();
            }
            catch (Exception) { }

            angry_birds_rio_music.Play(); // continue the music of the backgrownd.
            this.Show();
        }

        // כפתור קבלת סטטוס אישי
        private void myStatusButton_Click(object sender, EventArgs e)
        {
            angry_birds_rio_music.Stop(); // stop the music of the backgrownd.
            this.Hide();

            myStatus myStatus_window = new myStatus(clientStream, username);

            try
            {
                myStatus_window.ShowDialog();
            }
            catch (Exception) { }

            this.Show();
            angry_birds_rio_music.Play(); // continue the music of the backgrownd.
        }

        // כפתור הצטרפות לחדר קיים
        private void joinRoomButton_Click(object sender, EventArgs e)
        {
            isAdmin = false; // the user who joind the room is a regular user 

            joinRoom newJoinRoomWindow = new joinRoom(clientStream, username, isAdmin);

            angry_birds_rio_music.Stop(); // stop the music of the backgrownd.
            this.Hide();
            try
            {
                newJoinRoomWindow.ShowDialog();
            }
            catch (Exception) { };
            this.Show();
            angry_birds_rio_music.Play(); // continue the music of the backgrownd.
        }

        // כפתור יצירת חדר
        private void createRoomButton_Click(object sender, EventArgs e)
        {
            angry_birds_rio_music.Stop(); // stop the music of the backgrownd.

            isAdmin = true; // the user who created the room is the admin

            createRoom newRoom_window = new createRoom(clientStream, username, isAdmin);

            try
            {
                sawing_sound.Play();
                System.Threading.Thread.Sleep(1500); // wait because the next line is stop the "sawing" sound.
                hammering_sound.Play();

                this.Hide();

                newRoom_window.ShowDialog();
            }
            catch (Exception) { }

            this.Show();
            angry_birds_rio_music.Play(); // continue the music of the backgrownd.
        }

        private void home_Load(object sender, EventArgs e)
        {

        }
    }
}
