﻿namespace loadScrean
{
    partial class joinRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.currUsernameLabel = new System.Windows.Forms.Label();
            this.roomsListLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.roomsListPanel = new System.Windows.Forms.Panel();
            this.noRoomsLabel = new System.Windows.Forms.Label();
            this.usersInRoomLabel = new System.Windows.Forms.Label();
            this.usersInRoomPanel = new System.Windows.Forms.Panel();
            this.joinButton = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // currUsernameLabel
            // 
            this.currUsernameLabel.AutoSize = true;
            this.currUsernameLabel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.currUsernameLabel.Font = new System.Drawing.Font("Monotype Hadassah", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.currUsernameLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.currUsernameLabel.Location = new System.Drawing.Point(13, 13);
            this.currUsernameLabel.Name = "currUsernameLabel";
            this.currUsernameLabel.Size = new System.Drawing.Size(59, 22);
            this.currUsernameLabel.TabIndex = 0;
            this.currUsernameLabel.Text = "label1";
            // 
            // roomsListLabel
            // 
            this.roomsListLabel.AutoSize = true;
            this.roomsListLabel.BackColor = System.Drawing.Color.Black;
            this.roomsListLabel.Font = new System.Drawing.Font("Guttman Yad-Brush", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.roomsListLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.roomsListLabel.Location = new System.Drawing.Point(90, 68);
            this.roomsListLabel.Name = "roomsListLabel";
            this.roomsListLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.roomsListLabel.Size = new System.Drawing.Size(223, 36);
            this.roomsListLabel.TabIndex = 1;
            this.roomsListLabel.Text = "רשימת החדרים:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Font = new System.Drawing.Font("Guttman Vilna", 10F);
            this.button1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button1.Location = new System.Drawing.Point(674, 399);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "חזרה למסך הבית";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // roomsListPanel
            // 
            this.roomsListPanel.AutoScroll = true;
            this.roomsListPanel.Location = new System.Drawing.Point(66, 107);
            this.roomsListPanel.Name = "roomsListPanel";
            this.roomsListPanel.Size = new System.Drawing.Size(270, 91);
            this.roomsListPanel.TabIndex = 3;
            // 
            // noRoomsLabel
            // 
            this.noRoomsLabel.AutoSize = true;
            this.noRoomsLabel.BackColor = System.Drawing.Color.Black;
            this.noRoomsLabel.Font = new System.Drawing.Font("Guttman Yad-Brush", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.noRoomsLabel.ForeColor = System.Drawing.Color.Red;
            this.noRoomsLabel.Location = new System.Drawing.Point(38, 201);
            this.noRoomsLabel.Name = "noRoomsLabel";
            this.noRoomsLabel.Size = new System.Drawing.Size(334, 22);
            this.noRoomsLabel.TabIndex = 4;
            this.noRoomsLabel.Text = "!אם את/ה רוצה חדר, ייצר/י אותו בעצמך";
            this.noRoomsLabel.Visible = false;
            // 
            // usersInRoomLabel
            // 
            this.usersInRoomLabel.AutoSize = true;
            this.usersInRoomLabel.BackColor = System.Drawing.Color.Black;
            this.usersInRoomLabel.Font = new System.Drawing.Font("Guttman Yad-Brush", 20.25F);
            this.usersInRoomLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.usersInRoomLabel.Location = new System.Drawing.Point(323, 9);
            this.usersInRoomLabel.Name = "usersInRoomLabel";
            this.usersInRoomLabel.Size = new System.Drawing.Size(327, 36);
            this.usersInRoomLabel.TabIndex = 5;
            this.usersInRoomLabel.Text = ":החברים במשחק הנבחר";
            this.usersInRoomLabel.Visible = false;
            // 
            // usersInRoomPanel
            // 
            this.usersInRoomPanel.AutoScroll = true;
            this.usersInRoomPanel.BackColor = System.Drawing.Color.White;
            this.usersInRoomPanel.Location = new System.Drawing.Point(454, 68);
            this.usersInRoomPanel.Name = "usersInRoomPanel";
            this.usersInRoomPanel.Size = new System.Drawing.Size(132, 279);
            this.usersInRoomPanel.TabIndex = 6;
            this.usersInRoomPanel.Visible = false;
            // 
            // joinButton
            // 
            this.joinButton.BackColor = System.Drawing.Color.Black;
            this.joinButton.Enabled = false;
            this.joinButton.Font = new System.Drawing.Font("Guttman Vilna", 10F);
            this.joinButton.ForeColor = System.Drawing.Color.DodgerBlue;
            this.joinButton.Location = new System.Drawing.Point(166, 398);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(118, 34);
            this.joinButton.TabIndex = 7;
            this.joinButton.Text = "הצטרפות";
            this.joinButton.UseVisualStyleBackColor = false;
            this.joinButton.Click += new System.EventHandler(this.joinButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.Color.Black;
            this.refreshButton.Font = new System.Drawing.Font("Guttman Vilna", 10F);
            this.refreshButton.ForeColor = System.Drawing.Color.DodgerBlue;
            this.refreshButton.Location = new System.Drawing.Point(12, 398);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(118, 34);
            this.refreshButton.TabIndex = 8;
            this.refreshButton.Text = "נקה ועדכן";
            this.refreshButton.UseVisualStyleBackColor = false;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click_1);
            // 
            // joinRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.גםםר2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(804, 445);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.usersInRoomPanel);
            this.Controls.Add(this.usersInRoomLabel);
            this.Controls.Add(this.noRoomsLabel);
            this.Controls.Add(this.roomsListPanel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.roomsListLabel);
            this.Controls.Add(this.currUsernameLabel);
            this.Name = "joinRoom";
            this.Text = "joinRoom";
            this.Load += new System.EventHandler(this.joinRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label currUsernameLabel;
        private System.Windows.Forms.Label roomsListLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel roomsListPanel;
        private System.Windows.Forms.Label noRoomsLabel;
        private System.Windows.Forms.Label usersInRoomLabel;
        private System.Windows.Forms.Panel usersInRoomPanel;
        private System.Windows.Forms.Button joinButton;
        private System.Windows.Forms.Button refreshButton;
    }
}