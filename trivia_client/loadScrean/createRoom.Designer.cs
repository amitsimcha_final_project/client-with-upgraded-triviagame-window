﻿namespace loadScrean
{
    partial class createRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GoBackButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RoomNameLabel = new System.Windows.Forms.Label();
            this.NumOfPlayersLabel = new System.Windows.Forms.Label();
            this.NumOfQuestionsLabel = new System.Windows.Forms.Label();
            this.TimeForQuestionLabel = new System.Windows.Forms.Label();
            this.SendButton = new System.Windows.Forms.Button();
            this.RoomNameTextBox = new System.Windows.Forms.TextBox();
            this.NumOfPlayersTextBox = new System.Windows.Forms.TextBox();
            this.NumOfQuestionsTextBox = new System.Windows.Forms.TextBox();
            this.TimeForQuestionTextBox = new System.Windows.Forms.TextBox();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // GoBackButton
            // 
            this.GoBackButton.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.GoBackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.GoBackButton.ForeColor = System.Drawing.Color.Aqua;
            this.GoBackButton.Location = new System.Drawing.Point(545, 12);
            this.GoBackButton.Name = "GoBackButton";
            this.GoBackButton.Size = new System.Drawing.Size(75, 29);
            this.GoBackButton.TabIndex = 0;
            this.GoBackButton.Text = "Go Back";
            this.GoBackButton.UseVisualStyleBackColor = false;
            this.GoBackButton.Click += new System.EventHandler(this.GoBackButton_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(474, 253);
            this.label1.TabIndex = 1;
            // 
            // RoomNameLabel
            // 
            this.RoomNameLabel.AutoSize = true;
            this.RoomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomNameLabel.Location = new System.Drawing.Point(47, 60);
            this.RoomNameLabel.Name = "RoomNameLabel";
            this.RoomNameLabel.Size = new System.Drawing.Size(95, 16);
            this.RoomNameLabel.TabIndex = 2;
            this.RoomNameLabel.Text = "Room name:";
            // 
            // NumOfPlayersLabel
            // 
            this.NumOfPlayersLabel.AutoSize = true;
            this.NumOfPlayersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumOfPlayersLabel.Location = new System.Drawing.Point(47, 120);
            this.NumOfPlayersLabel.Name = "NumOfPlayersLabel";
            this.NumOfPlayersLabel.Size = new System.Drawing.Size(139, 16);
            this.NumOfPlayersLabel.TabIndex = 3;
            this.NumOfPlayersLabel.Text = "Number of players:";
            // 
            // NumOfQuestionsLabel
            // 
            this.NumOfQuestionsLabel.AutoSize = true;
            this.NumOfQuestionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumOfQuestionsLabel.Location = new System.Drawing.Point(47, 180);
            this.NumOfQuestionsLabel.Name = "NumOfQuestionsLabel";
            this.NumOfQuestionsLabel.Size = new System.Drawing.Size(154, 16);
            this.NumOfQuestionsLabel.TabIndex = 4;
            this.NumOfQuestionsLabel.Text = "Number of questions:";
            // 
            // TimeForQuestionLabel
            // 
            this.TimeForQuestionLabel.AutoSize = true;
            this.TimeForQuestionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeForQuestionLabel.Location = new System.Drawing.Point(47, 240);
            this.TimeForQuestionLabel.Name = "TimeForQuestionLabel";
            this.TimeForQuestionLabel.Size = new System.Drawing.Size(132, 16);
            this.TimeForQuestionLabel.TabIndex = 5;
            this.TimeForQuestionLabel.Text = "Time for question:";
            // 
            // SendButton
            // 
            this.SendButton.BackgroundImage = global::loadScrean.Properties.Resources.hammer;
            this.SendButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SendButton.Location = new System.Drawing.Point(317, 300);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(169, 83);
            this.SendButton.TabIndex = 6;
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // RoomNameTextBox
            // 
            this.RoomNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.RoomNameTextBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.RoomNameTextBox.Location = new System.Drawing.Point(249, 60);
            this.RoomNameTextBox.Name = "RoomNameTextBox";
            this.RoomNameTextBox.Size = new System.Drawing.Size(196, 23);
            this.RoomNameTextBox.TabIndex = 7;
            // 
            // NumOfPlayersTextBox
            // 
            this.NumOfPlayersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.NumOfPlayersTextBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.NumOfPlayersTextBox.Location = new System.Drawing.Point(249, 115);
            this.NumOfPlayersTextBox.Name = "NumOfPlayersTextBox";
            this.NumOfPlayersTextBox.Size = new System.Drawing.Size(196, 23);
            this.NumOfPlayersTextBox.TabIndex = 8;
            // 
            // NumOfQuestionsTextBox
            // 
            this.NumOfQuestionsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.NumOfQuestionsTextBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.NumOfQuestionsTextBox.Location = new System.Drawing.Point(249, 175);
            this.NumOfQuestionsTextBox.Name = "NumOfQuestionsTextBox";
            this.NumOfQuestionsTextBox.Size = new System.Drawing.Size(196, 23);
            this.NumOfQuestionsTextBox.TabIndex = 9;
            // 
            // TimeForQuestionTextBox
            // 
            this.TimeForQuestionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.TimeForQuestionTextBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.TimeForQuestionTextBox.Location = new System.Drawing.Point(249, 235);
            this.TimeForQuestionTextBox.Name = "TimeForQuestionTextBox";
            this.TimeForQuestionTextBox.Size = new System.Drawing.Size(196, 23);
            this.TimeForQuestionTextBox.TabIndex = 10;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.ErrorLabel.Location = new System.Drawing.Point(17, 331);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(294, 27);
            this.ErrorLabel.TabIndex = 11;
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.BackColor = System.Drawing.Color.SpringGreen;
            this.UserNameLabel.Font = new System.Drawing.Font("Aharoni", 15F, System.Drawing.FontStyle.Bold);
            this.UserNameLabel.Location = new System.Drawing.Point(68, 12);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(0, 20);
            this.UserNameLabel.TabIndex = 12;
            // 
            // createRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.building;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(632, 423);
            this.Controls.Add(this.UserNameLabel);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.TimeForQuestionTextBox);
            this.Controls.Add(this.NumOfQuestionsTextBox);
            this.Controls.Add(this.NumOfPlayersTextBox);
            this.Controls.Add(this.RoomNameTextBox);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.TimeForQuestionLabel);
            this.Controls.Add(this.NumOfQuestionsLabel);
            this.Controls.Add(this.NumOfPlayersLabel);
            this.Controls.Add(this.RoomNameLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GoBackButton);
            this.Name = "createRoom";
            this.Text = "Create Room";
            this.Load += new System.EventHandler(this.createRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GoBackButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label RoomNameLabel;
        private System.Windows.Forms.Label NumOfPlayersLabel;
        private System.Windows.Forms.Label NumOfQuestionsLabel;
        private System.Windows.Forms.Label TimeForQuestionLabel;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.TextBox RoomNameTextBox;
        private System.Windows.Forms.TextBox NumOfPlayersTextBox;
        private System.Windows.Forms.TextBox NumOfQuestionsTextBox;
        private System.Windows.Forms.TextBox TimeForQuestionTextBox;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label UserNameLabel;
    }
}